#!/bin/bash

build_tag () {
  tag_extention=""
  if [[ -n $1 ]]; then
    tag_extention="-$1"
  fi
  tag=$(basename "$(pwd)")$tag_extention:$(git rev-parse --abbrev-ref HEAD)
  tag="$(echo "$tag" | sed 's/\//_/g')"
  echo "$tag"
}