# Analyzer Common README

Below is a set of build and run instructions that works for all analyzers. Please read through them carefully to understand how to setup, run, debug, and test your analyzer locally.

### Setting up for local development

You will need to do the following before getting started:

1. Ensure Docker is installed and configured correctly on your machine.
1. Clone the [Analyzer Scripts](https://gitlab.com/gitlab-org/secure/tools/analyzer-scripts) repository and add it to your `PATH` (optional, but recommended).
1. Test that it has been added to your `PATH` by opening a new terminal window and executing `analyzer-build`. You should see some help text outputted to the terminal.
1. Clone the analyzer repository.

**Please note**: it's totally possible to build and run analyzer images manually (using docker build and docker run commands), but
it's highly recommended to use the scripts provided, as they take care of configuring bind mounts and setting sensible defaults for the environment variables you need.

### Running the analyzer

To run the analyzer locally, you'll use the `analyzer-run` script that you cloned from the [Analyzer Scripts](https://gitlab.com/gitlab-org/secure/tools/analyzer-scripts) repository in the
previous step. Most analyzers come with a set of mock repositories in the `qa/fixtures` folder which you can use as test projects. If you can't see a `qa/fixtures` folder, you can either:

* Clone a relevant repository from the [tests](https://gitlab.com/gitlab-org/security-products/tests) subgroup.
* Create a directory on your machine with your own source code.

Once you have a test project in mind:

1. `cd` into the directory where you checked out this repository.
1. Execute `analyzer-build`. This will build a fresh Docker image based on the current checkout. The image is tagged automatically with the name of the directory and branch.
1. Execute `analyzer-run <path-to-test-project>`, providing the path to a test project as an argument. For example, `analyzer-run ./qa/fixtures/java-android` will run the analyzer against the `java-android` test project within the `qa/fixtures` directory.
1. If the run was successful, you should see the appropriate `gl-*.json` report created within the test project directory.

### Debugging the analyzer

Most analyzers can be debugged interactively using [`delve`](https://github.com/go-delve/delve). This takes place in a _debug_ container which the [Analyzer Scripts](https://gitlab.com/gitlab-org/secure/tools/analyzer-scripts) will attempt to build for you. This container wraps the actual analyzer image and installs the `delve` tool and OS dependencies.

Caveats to debugging:

- `analyzer-debug` assumes that the analyzer image uses an Alpine base. Non-Alpine analyzers cannot be debugged with this tool.
- Debugging is not guaranteed to work for all analyzers, and some may work in 95% of cases but crash unexpectedly.
- Issues are compounded if you happen to be running on Apple Silicon or another Arm-based host machine.

Again, to debug an analyzer, choose a test project, and then:

1. `cd` into the directory where you checked out this repository.
1. Execute `analyzer-build`. This will be used as the base for the debug image.
1. Execute `analyzer-debug <path-to-test-project>` to build and run the debug image.
1. If all goes well, you'll be dropped into the shell of the running debug container.

There's a few things you can now do:

1. Freely browse the filesystem of the analyzer container. It's set up similarly to how it'd run in CI; the test project is mounted at `/tmp/app` and the analyzer executable is available at `/analyzer`.
1. Run the analyzer normally, by executing `/analyzer run` or `/analyzer --help` for a list of available commands.
1. Start a `delve` session to interactively debug the analyzer source. Within the `/go/src/app` directory, execute `dlv debug -- run` (arguments after `--` are passed as arguments to the Go program). `delve` will compile the Go source and drop you into a new session. Consult the [documentation](https://github.com/go-delve/delve/tree/master/Documentation) on how to use `delve`.

### Testing the analyzer

Analyzers typically include two kinds of tests:

1. Unit tests, within the `*_test.go` files.
2. Integration tests, expressed as a [RSpec](https://rspec.info/) file inside the `spec/` directory.

#### Unit tests

Unit tests are executed in the CI pipeline of the analyzer itself. You can also run these locally within a debug container, or on your host machine if you have the Go toolchain installed. Simply execute `go test ./...`.

#### Integration tests

Image integration tests are executed on CI to check the Docker image of the analyzer using [RSpec](https://rspec.info/). They check the output and exit code of the analyzer, as well as the SAST report it generates.

You can run these tests locally using the Analyzer Scripts:

1. `cd` into the directory where you checked out this repository.
1. Execute `analyzer-integration-rspec`. This will run `analyzer-build` for you, and then kick off the [integration-test](https://gitlab.com/gitlab-org/security-products/analyzers/integration-test) runner against RSpec files in `spec/`.
